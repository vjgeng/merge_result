Installation


<ul>
	step 1: clone the project with the command git clone <strong>https://gitlab.com/vjgeng/merge_result.git</strong>
</ul>
<ul>
	step 2: open the project from the command prompt or ide tool such as visual studio
</ul>

<ul>
	step 3: to get the dependencies, you need to run the <strong><code style="color : aqua">npm install</code></strong> command
</ul>

<ul>
	step 4: the user can test the application via html name index.html and the unit test that provided in seperate file in the project, the picture below is the page when launch
 for the first time
</ul>


<ul>
<img src="../images/initial.jpg" alt="Alt text" title="test scenario 1" width="600" 
     height="500">
</ul>

<ul>
	step 5: when click on the sort button, it will show the final result of the array list
</ul>

<ul> 
<img src="../images/result_html.jpg" alt="Alt text" title="test scenario 1" width="600" 
     height="500">
</ul>

<ul> 
	if the user want to change the data in the array, then it can be made by opening file sandbox.ts and change the data in array as show in the image below and you need to run the command 
	<strong><code style="color : aqua">tsc sandbox.ts</code></strong> in order to compile the typescript file and have a change reflect in the javascript file
</ul>

<ul> 
<img src="../images/modify_data.jpg" alt="Alt text" title="test scenario 1"
width="600" 
     height="500">
</ul>

<ul> 
	Another way to test the result of the applicatin is via the unit test itselt, you can open the file <strong><code style="color : aqua">merge.test.ts</code></strong> and then you can change the data in the array similar way that you do in the 
	<strong><code style="color : aqua">sandbox.ts</code></strong> in the previous step
</ul>

<ul> 
<img src="../images/test_2.jpg" alt="Alt text" title="test scenario 1"
width="600" 
     height="500">
</ul>

<ul> 
	finally, when you want to run the test unit, you need to open the terminal in visual studio ide and then navigate to the merge_result project and with 
	the project run <strong><code style="color : aqua">npm test</code></strong> command and it will give the test result as show below
</ul>

<ul> 
<img src="../images/unit_test_result.jpg" alt="Alt text" title="test scenario 1"
width="600" 
     height="500">
</ul>

