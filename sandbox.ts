
interface isMerge
{ 

  merge(collection_1:number[], collection_2:number[]) :number[];

}


export class useMerge implements isMerge{

  merge(collection_1:number[], collection_2:number[]) { 
    return collection_1.concat(this.sort_merge(collection_2));
  }

  sort_merge(collection_2:number[]) :number[]{
    var done = false;
    while (!done) {
      done = true;
      for (var i = 1; i < collection_2.length; i += 1) {
        if (collection_2[i - 1] > collection_2[i]) {
          done = false;
          var tmp = collection_2[i - 1];
          collection_2[i - 1] = collection_2[i];
          collection_2[i] = tmp;
        }
      }
    }
  
    return collection_2;
  }

}

var nums:number[] = [1,2,3,3];
var nums2:number[] = [6,3,5,1,2];

const result_merge = new useMerge();

function printMerger(){

  var inputF = document.getElementById("merger_result") as HTMLInputElement ;

  let final_result = result_merge.merge(nums, nums2).toString();

  if(inputF != null) {
    inputF.value=final_result;
}

}