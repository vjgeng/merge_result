"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useMerge = void 0;
var useMerge = /** @class */ (function () {
    function useMerge() {
    }
    useMerge.prototype.merge = function (collection_1, collection_2) {
        return collection_1.concat(this.sort_merge(collection_2));
    };
    useMerge.prototype.sort_merge = function (collection_2) {
        var done = false;
        while (!done) {
            done = true;
            for (var i = 1; i < collection_2.length; i += 1) {
                if (collection_2[i - 1] > collection_2[i]) {
                    done = false;
                    var tmp = collection_2[i - 1];
                    collection_2[i - 1] = collection_2[i];
                    collection_2[i] = tmp;
                }
            }
        }
        return collection_2;
    };
    return useMerge;
}());
exports.useMerge = useMerge;
var nums = [1, 2, 3, 3];
var nums2 = [6, 3, 5, 1, 2];
var result_merge = new useMerge();
function printMerger() {
    var inputF = document.getElementById("merger_result");
    var final_result = result_merge.merge(nums, nums2).toString();
    if (inputF != null) {
        inputF.value = final_result;
    }
}
